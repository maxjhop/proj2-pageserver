from flask import Flask, render_template, request, abort
import os

app = Flask(__name__)

@app.route("/")
def index():
    return "Main page"

@app.route("/<name>")
def others(name):

    #check for 403 error
    string = request.environ["REQUEST_URI"]
    string = string[1:]
    directories = string.split("/")
    
    for i in range(len(directories)):
        if(directories[i] == '' or directories[i][0] == "~" or directories[i][0:2] == ".."):
            abort(403)

    #try to push template, if it doesnt exist then throw 404 error
    try:
        return render_template(name)
    except:
        abort(404)
    
@app.errorhandler(404)
def error_404(e):
    #piazza helped me figure this out, just copied 403 test from above
    string = request.environ["REQUEST_URI"]
    string = string[1:]
    directories = string.split("/")
    
    for i in range(len(directories)):
        if(directories[i] == '' or directories[i][0] == "~" or directories[i][0:2] == ".."):
            return render_template('403.html'), 403

    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
